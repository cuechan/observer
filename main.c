#include <pcap/pcap.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>



#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"


char errbuf[PCAP_ERRBUF_SIZE];

void print_interfaces() {
	pcap_if_t* interfaces;

	printf("available devices:\n");
	if (pcap_findalldevs(&interfaces, errbuf) == PCAP_ERROR) {
		perror("can't find devices");
	}

	while (interfaces != NULL) {
		printf("%s\n", interfaces->name);
		pcap_addr_t* addrs = interfaces->addresses;

		// FIXME: ipv4 addresses are not printed correctly
		while (addrs != NULL) {
			char addressstring[255];
			memset(addressstring, 0, 255);

			inet_ntop(addrs->addr->sa_family, addrs->addr, addressstring, 255);
			printf("  %s\n", addressstring);

			addrs = addrs->next;
		}

		interfaces = interfaces->next;
	}
}



void print_supported_layers(pcap_t* handle) {
	printf("available linklayer datatypes\n");

	int *dlt_buf;
	int layers = pcap_list_datalinks(handle, &dlt_buf);

	if (layers == PCAP_ERROR) {
		printf("can't get supported layers\n");
		exit(1);
	}

	if (layers == PCAP_ERROR_NOT_ACTIVATED) {
		printf("interface is not activated\n");
		exit(1);
	}

	for (size_t i = 0; i < layers; i++) {
		printf("linklayertype: %s\n", pcap_datalink_val_to_name(dlt_buf[i]));
	}
}


void packet_handler(u_char* user, const struct pcap_pkthdr* h, const u_char* bytes) {
	printf(
		"%s[%06li.%06li]%s packet length %d bytes\n",
		ANSI_COLOR_GREEN,
		h->ts.tv_sec,
		h->ts.tv_usec,
		ANSI_COLOR_RESET,
		h->len
	);
}




int main(int argc, char* argv[]) {
	if (argc < 2) {
		print_interfaces();
		exit(0);
	}

	char* iface = (char*) malloc(strlen(argv[1]) * sizeof(char));
	iface = strcpy(iface, argv[1]);

	printf("open interface %s\n", iface);


	pcap_t* handle;
	if ((handle = pcap_create(iface, errbuf)) == NULL) {
		printf("open interface %s failed: %s\n", iface, errbuf);
		exit(1);
	}




	pcap_set_promisc(handle, 1);
	if (pcap_activate(handle)) {
		perror("error activating interface");
		exit(1);
	}

	print_supported_layers(handle);


	pcap_loop(handle, -1, packet_handler, NULL);



	exit(0);
}
